<?php

use Faker\Generator as Faker;

$factory->define(App\Producto::class, function (Faker $faker) {
    return [
        'name'=>$faker->name,
        'url'=>$faker->imageUrl($width = 640, $height = 480),
        'email'=>$faker->email,
        'content'=>$faker->text(200),
    ];
});
