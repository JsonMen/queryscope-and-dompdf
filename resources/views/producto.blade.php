<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">


    <title>Document</title>
</head>
<body>

    <div class="container">
        <div class="row">
            {!! Form::open(['route'=>'producto.index','method'=>'GET']) !!}
                <div class="card-body row no-gutters align-items-center">

                    <!--end of col-->
                    <div class="col" style="margin-right: 10px;">

                        {!! Form::Select('paginate',['15'=>'15','20'=>'20','30'=>'30'],null,['class'=>'form-control']) !!}

                    </div>
                    <div class="col" style="margin-right: 10px;">
                        {!! Form::text('id',null,['class'=>'form-control','type'=>'search','placeholder'=>'Id']) !!}
                    </div>
                    <div class="col" style="margin-right: 10px;">
                        {!! Form::text('name',null,['class'=>'form-control','type'=>'search','placeholder'=>'Nombre']) !!}

                    </div>
                    <div class="col" style="margin-right: 10px;">
                        {!! Form::text('email',null,['class'=>'form-control','type'=>'search']) !!}
                    </div>

                    <!--end of col-->
                    <div class="col-auto" style="margin-right: 10px;">
                        {!! Form::submit('Buscar',['class'=>'btn btn-primary']) !!}
                    </div>
                    <!--end of col-->
                    <div class="col-auto">
                        <a href="" class="btn btn-danger">PDF</a>
                    </div>

                </div>
            {!! Form::close() !!}

            <!--end of col-->
        </div>
        @if(!empty($producto))
        <table class="table table-hover">
            <thead>
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Name</th>
                <th scope="col">Image</th>
                <th scope="col">Email</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            @foreach($producto as $pro)
                <tr>
                    <th>{{ $pro->id }}</th>
                    <th scope="row">{{ $pro->name }}</th>
                    <td><img src="{{asset('/css/logo.svg')}}" alt="" style="width: 40px;height: 40px;"></td>
                    <td>{{$pro->email}}</td>

                    <th scope="col"><a href="{{ route('producto.show',$pro->id) }}"><i class="far fa-file-pdf"></i></a></th>
                </tr>
            @endforeach
            </tbody>
        </table>
        {!! $producto->render() !!}
        @endif
    </div>
</body>
</html>

