<?php

namespace App\Http\Controllers;

use App\Producto;
use Illuminate\Http\Request;


class ProductoController extends Controller
{
    public function index(Request $request)
    {
       $name=$request->get('name');
       $email=$request->get('email');
       $id=$request->get('id');
       if($request->get('paginate')){
          $pag=$request->get('paginate');
       }
       else
           $pag=15;
       //dd($name);
       $producto=Producto::orderBy('id','ASC')
           //->where('name','LIKE',"%$name%")
           ->Name($name)
           ->Id($id)
           ->Email($email)
           ->paginate($pag);
       return view('producto',compact('producto','pag','email'));
    }
    public function show($id)
    {
        $Producto = Producto::where('id','=',$id)->first();
        $pdf = \PDF::loadView('pdf.index', compact('Producto'));

        return $pdf->download('listado.pdf');
        //$pdf->loadHTML('<h1>Test</h1>');
        //return $pdf->stream();
        /*
        //dd($consulta);
        $view= \View::make('pdf.index',compact('consulta'))->render();
        $pdf = \App::make('dompdf.wrapper');

        //$pdf = \PDF::loadView( $view, compact('data') );

        $pdf->loadHTML($view);

        return $pdf->stream();*/

    }


}
