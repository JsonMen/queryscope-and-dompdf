<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $table='producto';
    protected $fillable=['name','url','email','content'];
    public $timestamps=false;
    // scopes

    public function scopeName($query,$name){
        if($name)
            return $query->where('name','LIKE',"%$name%");
    }
    public function scopeEmail($query,$email){
        if($email)
            return $query->where('email','LIKE',"%$email%");
    }
    public function scopeId($query,$id){
        if($id)
            return $query->where('id','LIKE',"%$id%");
    }
}
